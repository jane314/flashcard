/*
 *  flashcards
 *  by janie davis
 *
 */

/* 
 * types
 */
interface entry {
	q: string;
	a: string;
}

interface entry_data {
	key: string;
	entry: entry;
}

interface card_data {
	[key: string]: entry;
}

/*
 * auxiliary functions
 */
function randi(n: number): number {
	return Math.floor(Math.random() * n);
}

function rand_entry(obj: card_data): entry_data {
	let keys: string[] = Object.keys(obj);
	let index: number = randi(keys.length);
	return {'key': keys[index], 'entry':obj[keys[index]]};
}

function save(obj: card_data): void {
	  try {
		  let save_file_name = './saved/cards.' + (new Date).getTime() + '.json';
		  Deno.writeTextFileSync(save_file_name, JSON.stringify(obj));
		  console.log("saved current stack to ", save_file_name);
	  } catch (e) {
		 console.log(e); 
	  }
}

/*
 *  process data from csv file 
 */
if ( Deno.args.length != 1) {
	console.log("USAGE: ./flashcard.sh /path/cards.csv");
	Deno.exit();
}

const filename: string = Deno.args[0];
var data: any;

/*
 * load file depending on if its file extension is CSV or JSON. 
 * Assumes good input from user
 */
if( filename.toUpperCase().search("CSV") != -1 ) {
	let text: string = await Deno.readTextFile(filename);
	let data_tmp: string[] = text.split('\n');
	// filter empty lines and commented lines
	data_tmp = data_tmp.filter( t => { 
		if( t == "" ) {
			return 0;
		}
		return (t[0] != '#');
	});
	data = {...data_tmp.map( function (csvstr: string): entry {
		let tmp: number = csvstr.search(';'); 
		return {'q':csvstr.substring(0,tmp), 'a':csvstr.substring(tmp + 1)}; 
		}
	)};
} else if( filename.toUpperCase().search("JSON") != -1) {
	let	text = await Deno.readTextFile(filename);
	data = JSON.parse(text);
} else {
	console.log("Need a JSON or CSV file.");
	console.log("USAGE: ./flashcard.sh /path/cards.csv");
	Deno.exit();
}

const reset_data: object = JSON.parse(JSON.stringify(data));

/*
 * main flashcard studying loop
 */
while(Object.keys(data).length > 0) {

	let rand_entry_data: entry_data = rand_entry(data);
	let key: string = rand_entry_data.key;
	let entry: entry = rand_entry_data.entry;
	let buf: any = new Uint8Array(10);
	console.log('--------------------');
	console.log(entry.q);
	await Deno.stdin.read(buf);
	console.log(entry.a);
	await Deno.stdin.read(buf);
	
	// process command after answer is shown
	// d = delete / done
	if ( buf[0] == 68 || buf[0] == 100 ) { 
		delete data[key];
		console.log('✅');	
	}
	// s = save
	else if (buf[0] == 83 || buf[0]==115) {
		save(data);	
	} 
	// q = quit	
	else if( buf[0] == 81 || buf[0] == 113 ) {
		Deno.exit();	
	}
	// r = reset
	else if( buf[0] == 82 || buf[0] == 114 ) {
		data = JSON.parse(JSON.stringify(reset_data));
		console.log("stack is reset to the stack from ", filename);	
	}
	else {}

}

console.log("\nGOOD WORK!");
	


