Flashcards
-------------------

To install:
- Enter a Unix environment and navigate to where you'd like to put this app's folder.
- Run `git clone https://gitlab.com/jane314/flashcard.git`
- Run `cd ./flashcard` 
- Run `chmod u+x ./setup.sh`
- Run `./setup.sh`
	- If you get a message like "unzip is required", install the relevant package with your package manager. Other than that, this setup script should do everything you need.
- Run `exec bash` to reload your bash profile.

To setup and run:
- Add your flash card entries to a `.csv` file, using the format shown in the example `cards.csv` file provided. One flash card entry per line, each line with the format `question;answer`.
- Run `./flashcard.sh cards.csv`
(You can replace `cards.csv` by your own CSV file's name, or a JSON file which is saved via this app's save command.)
- Keep hitting `Enter` to quiz yourself on your flash cards. After an answer is shown, you can run the following commands:
	- **`S`** key: **save** current card stack to a JSON file in the `saved` directory.
	- **`D`** key: **delete** current card from the stack. (I.e., you know this flashcard! You don't need to be quizzed on it anymore.)
	- **`R`** key: **reset** to the original stack.
	- **`Q`** key: **quit** the app.

CSV File Format:
 - Good input from your flashcard CSV files is important. This doesn't have much error checking.
 - The main thing is: the CSV file should only have one line per file, each line having a question and answer separated by a `;` delimiter. 
 - Escaping `;` will not work in the CSV file. Each line is split between question and answer at the first occurance of a `;`.
 - You can comment out a line by making its first character a `#`. (No leading space before the `#`.)
 - UPDATE: This app is no longer sensitive to blank lines in the CSV file.

 Todo:
 - Currently when you pass `flashcard.sh` a file that doesn't exist, it gives you a Typescript-level error, instead of a nice friendly error message.
