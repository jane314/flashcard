#!/usr/bin/sh
chmod u+x ./flashcard.sh
echo "export DENO_INSTALL=\"/home/$USER/.deno\"" >>~/.bashrc
echo "export PATH=\"$DENO_INSTALL/bin:$PATH\"" >>~/.bashrc
mkdir saved
curl -fsSL https://deno.land/x/install/install.sh | sh
